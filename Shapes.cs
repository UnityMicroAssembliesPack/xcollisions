﻿using UnityEngine;

public static partial class XCollision
{
    //TODO: Move logic to overlaps!

    public static class Shapes {
        public interface IShape {
            bool setFromCollider(Collider inCollider);

            Vector3 center { get; set; }
            void translate(Vector3 inDistance);

            void overlap(FastArray<Collider> outColliders, int inLayerMask = Physics.AllLayers);
            bool trace(out RaycastHit outHit, Vector3 inDirection, int inLayerMask = Physics.AllLayers);
            bool traceSeparate(out RaycastHit outHit, Collider inCollider, Vector3 inDirection);
        }

        public struct Point : IShape {
            public Point(Vector3 inPoint) {
                _point = inPoint;
            }

            public bool setFromCollider(Collider inCollider) {
                return false;
            }

            public Vector3 center {
                get { return _point; }
                set { _point = value; }
            }

            public void translate(Vector3 inDistance) {
                _point += inDistance;
            }
            
            public void overlap(FastArray<Collider> outColliders, int inLayerMask = Physics.AllLayers) {
                XCollision.overlap(outColliders, this, inLayerMask);
            }

            public bool trace(out RaycastHit outHit, Vector3 inDirection, int inLayerMask = Physics.AllLayers) {
                return XCollision.trace(out outHit, this, inDirection, inLayerMask);
            }

            public bool traceSeparate(out RaycastHit outHit, Collider inCollider, Vector3 inDirection) {
                return XCollision.traceSeparate(out outHit, inCollider, this, inDirection);                
            }

            private Vector3 _point;
        }

        public struct Sphere : IShape {
            public Sphere(Vector3 inCenter, float inRadius) {
                _center = inCenter;
                _radius = inRadius;
            }

            public Sphere(SphereCollider inSphereCollider) {
                XCollisionPrivate.checkCollider(inSphereCollider);
                Transform theTransform = inSphereCollider.transform;

                _center = theTransform.TransformPoint(inSphereCollider.center);
                _radius = theTransform.lossyScale.x * inSphereCollider.radius;
            }

            public bool setFromCollider(Collider inCollider) {
                var theSphereCollider = inCollider as SphereCollider;
                if (!XUtils.isValid(theSphereCollider)) return false;

                this = new Sphere(theSphereCollider);
                return true;
            }

            public bool isZero() { return 0.0f == radius; }

            public Vector3 center {
                get { return _center; }
                set { _center = value; }
            }

            public float radius {
                get { return _radius; }
                set { _radius = value; }
            }

            public void translate(Vector3 inDistance) {
                center += inDistance;
            }
            
            public void overlap(FastArray<Collider> outColliders, int inLayerMask = Physics.AllLayers) {
                XCollision.overlap(outColliders, this, inLayerMask);
            }

            public bool trace(out RaycastHit outHit, Vector3 inDirection, int inLayerMask = Physics.AllLayers) {
                return XCollision.trace(out outHit, this, inDirection, inLayerMask);
            }

            public bool traceSeparate(out RaycastHit outHit, Collider inCollider, Vector3 inDirection) {
                return XCollision.traceSeparate(out outHit, inCollider, this, inDirection);                
            }

            public Vector3 _center;
            public float _radius;
        }

        public struct Capsule : IShape {
            public Capsule(Vector3 inPoint1, Vector3 inPoint2, float inRadius) {
                _point1 = inPoint1;
                _point2 = inPoint2;
                _radius = inRadius;
            }

            public Capsule(CapsuleCollider inCapsuleCollider) {
                XCollisionPrivate.checkCollider(inCapsuleCollider);
                Transform theTransform = inCapsuleCollider.transform;

                Vector3 theHeightDirection = new Vector3();
                switch (inCapsuleCollider.direction) {
                    case 0/*x*/:  theHeightDirection = new Vector3(1.0f, 0.0f, 0.0f);  break;
                    case 1/*y*/:  theHeightDirection = new Vector3(0.0f, 1.0f, 0.0f); break;
                    case 2/*z*/:  theHeightDirection = new Vector3(0.0f, 0.0f, 1.0f);  break;
                }
                Vector3 theHalfDirectionLocal = theHeightDirection * inCapsuleCollider.height / 2;

                Vector3 theCenter = inCapsuleCollider.center;
                _point1 = theTransform.TransformPoint(inCapsuleCollider.center + theHalfDirectionLocal);
                _point2 = theTransform.TransformPoint(inCapsuleCollider.center - theHalfDirectionLocal);
                _radius = theTransform.lossyScale.x * inCapsuleCollider.radius;
            }

            public bool setFromCollider(Collider inCollider) {
                var theCapsuleCollider = inCollider as CapsuleCollider;
                if (!XUtils.isValid(theCapsuleCollider)) return false;

                this = new Capsule(theCapsuleCollider);
                return true;
            }

            public bool isSphere() { return point1 == point2; }
            public bool isSegment() { return 0.0f == radius; }
            public bool isZero() { return isSphere() && isSegment(); }

            public Vector3 point1 {
                get { return _point1; }
                set { _point1 = value; }
            }

            public Vector3 point2 {
                get { return _point2; }
                set { _point2 = value; }
            }

            public float radius {
                get { return _radius; }
                set { _radius = value; }
            }

            public Vector3 center {
                get { return _point1 + halfHeightDelta; }
                set {
                    Vector3 theHalfHeightDelta = halfHeightDelta;
                    _point1 = value - theHalfHeightDelta;
                    _point2 = value + theHalfHeightDelta;
                }
            }

            public void translate(Vector3 inDistance) {
                point1 += inDistance;
                point2 += inDistance;
            }

            public void overlap(FastArray<Collider> outColliders, int inLayerMask = Physics.AllLayers) {
                XCollision.overlap(outColliders, this, inLayerMask);
            }

            public bool trace(out RaycastHit outHit, Vector3 inDirection, int inLayerMask = Physics.AllLayers) {
                return XCollision.trace(out outHit, this, inDirection, inLayerMask);
            }

            public bool traceSeparate(out RaycastHit outHit, Collider inCollider, Vector3 inDirection) {
                return XCollision.traceSeparate(out outHit, inCollider, this, inDirection);
            }

            private Vector3 halfHeightDelta {
                get { return (_point2 - _point1)/2; }
            }

            private Vector3 _point1;
            private Vector3 _point2;
            private float _radius;
        }

        //TODO: Make default value with not zero Quaternion
        public struct Box : IShape {
            public Box(Vector3 inCenter, Vector3 inHalfExtents, Quaternion inRotation) {
                _center = inCenter;
                _halfExtents = inHalfExtents;
                _rotation = inRotation;
            }

            public Box(BoxCollider inBoxCollider) {
                XCollisionPrivate.checkCollider(inBoxCollider);
                Transform theTransform = inBoxCollider.transform;

                _center = theTransform.TransformPoint(inBoxCollider.center);
                _halfExtents = theTransform.TransformDirection(inBoxCollider.size / 2) * theTransform.lossyScale.x;
                _rotation = theTransform.rotation;
            }

            public Vector3 center {
                get { return _center; }
                set { _center = value; }
            }

            public Vector3 halfExtents {
                get { return _halfExtents; }
                set { _halfExtents = value; }
            }

            public Quaternion rotation {
                get { return _rotation; }
                set { _rotation = value; }
            }

            public bool setFromCollider(Collider inCollider) {
                var theBoxCollider = inCollider as BoxCollider;
                if (!XUtils.isValid(theBoxCollider)) return false;

                this = new Box(theBoxCollider);
                return true;
            }

            public bool isZero() { return (Vector3.zero == halfExtents); }

            public void translate(Vector3 inDistance) {
                center += inDistance;
            }
            
            public void overlap(FastArray<Collider> outColliders, int inLayerMask = Physics.AllLayers) {
                XCollision.overlap(outColliders, this, inLayerMask);
            }

            public bool trace(out RaycastHit outHit, Vector3 inDirection, int inLayerMask = Physics.AllLayers) {
                return XCollision.trace(out outHit, this, inDirection, inLayerMask);
            }

            public bool traceSeparate(out RaycastHit outHit, Collider inCollider, Vector3 inDirection) {
                return XCollision.traceSeparate(out outHit, inCollider, this, inDirection);                
            }

            private Vector3 _center;
            private Vector3 _halfExtents;
            private Quaternion _rotation;
        }
    }

    public static T_Type translated<T_Type>(this T_Type thisShape, Vector3 inDistance)
        where T_Type : struct, Shapes.IShape
    {
        T_Type theResult = thisShape; theResult.translate(inDistance);
        return theResult;
    }

    public static bool isPoint(this Collider inCollider) {
        XUtils.check(inCollider);
        if (inCollider.bounds.size.x >= XCollisionPrivate.kMinimalSphereRadius) return false;
        if (inCollider.bounds.size.y >= XCollisionPrivate.kMinimalSphereRadius) return false;
        if (inCollider.bounds.size.z >= XCollisionPrivate.kMinimalSphereRadius) return false;
        return true;
    }
}
