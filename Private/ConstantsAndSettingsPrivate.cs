﻿internal static partial class XCollisionPrivate
{
    public static float kMinimalSphereRadius = 0.001f;

    public const int kMaxHitsToTrack = 20;
    public const int kMaxCollidersToTrack = 20;
}
