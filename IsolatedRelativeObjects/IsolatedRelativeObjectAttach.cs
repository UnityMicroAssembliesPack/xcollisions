﻿using UnityEngine;
using static XUtils;
using XMathTypes;

//Methods
//TODO: Replace with "OnParentChanged" from Live Edit Object
[ExecuteAlways]
public class IsolatedRelativeObjectAttach : MonoBehaviour
{
#   if UNITY_EDITOR
    private void Update() {
        if (Application.IsPlaying(this)) return;
        updateHoldedObjectTransform();
    }
#   endif

    private void FixedUpdate() {
        updateHoldedObjectTransform();
    }

    private void updateHoldedObjectTransform() {
        GameObject theHoldedObject = holdedObject;
        if (!isValid(theHoldedObject)) return;

        IsolatedRelativeObjectsGroup theGroup = group;
        if (theGroup && _isolationEnabled) {
            attachToGroup(theGroup);

            var theThisNewGlobalTransform = new LocatorTransform(transform);
            var theHoldedObjectNewTransform = theGroup.getGroupLocatorTransform(theThisNewGlobalTransform);
            var theHoldedObjectOldTransform = new LocatorTransform(theHoldedObject.transform);

            if (!XMath.equalsWithPrecision(theHoldedObjectNewTransform, theHoldedObjectOldTransform)) {
                theHoldedObjectNewTransform.applyForTransform(theHoldedObject.transform);
            }
        } else {
            if (theHoldedObject.transform.parent != transform) {
                theHoldedObject.transform.SetParent(transform);
                XMath.resetTransform(theHoldedObject.transform);
            }
        }
    }

    void attachToGroup(IsolatedRelativeObjectsGroup inGroup) {
        if (null != _holdedObject.transform.parent) {
            _holdedObject.transform.SetParent(null);
        }
    }

    internal GameObject holdedObject { get { return _holdedObject; } }

    internal IsolatedRelativeObjectsGroup group {
        get { return getComponentInParent<IsolatedRelativeObjectsGroup>(this); }
    }

    //Fields
    [SerializeField] private GameObject _holdedObject = null;
    [SerializeField] private bool _isolationEnabled = true;
}
