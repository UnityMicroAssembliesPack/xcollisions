﻿using UnityEngine;
using static XUtils;

public static partial class XCollision
{
    // -------------------------------- Event -------------------------------------

    public enum TraceEventType {
        StartInside,
        Exit,
        Enter,
        FinishInside
    }

    public struct TraceEvent {
        public TraceEvent(TraceEventType inType, Collider inCollider,
            Optional<RaycastHit> inHit = new Optional<RaycastHit>())
        {
            type = inType;
            collider = inCollider;
            hit = inHit;
            traceDirection = Vector3.zero;
        }

        public TraceEventType type;
        public Collider collider;
        public Optional<RaycastHit> hit;
        public Vector3 traceDirection;
    }

    // -------------------------------- Trace logic -------------------------------------

    public enum TraceSettingFlag {
        CastInsideColliders,
        TrackTraceMedium
    }

    public abstract class ITraceLogic {
        protected void reset() {
            _sweepShifts.clear(false);
            _traceLayerMask = 0;
            _collidersToIgnore.clear(false);
            _flags.setAll(false);
        }

        protected void addShift(Vector3 inShift) {
            _sweepShifts.add(inShift);
        }

        protected void addShifts(FastArray<Vector3> inShifts) {
            _sweepShifts.addAll(inShifts);
        }

        protected void setTraceLayerMask(int inMask) {
            _traceLayerMask = inMask;
            XCollisionPrivate.preventUsingUtilityLayers(ref _traceLayerMask);
        }

        protected void addColliderToIgnore(Collider inCollider) {
            check(inCollider);
            _collidersToIgnore.add(inCollider);
        }

        protected void addObjectToIgnore(GameObject inGameObject) {
            check(inGameObject);
            collectComponentsInChildren(_collidersToIgnore, inGameObject);

            //Debug.Log("{");
            //foreach (Collider theCollider in _collidersToIgnore) {
            //    Debug.Log(theCollider.gameObject.name);
            //}
            //Debug.Log("}");
        }

        protected void addFlag(TraceSettingFlag inFlags) {
            _flags.set(inFlags, true);
        }

        protected internal abstract bool testFinishCastOnEvent(TraceEvent inEvent);

        internal EnumBitMask<TraceSettingFlag> _flags;

        internal FastArray<Vector3> _sweepShifts = new FastArray<Vector3>();
        internal LayerMask _traceLayerMask = Physics.AllLayers;
        internal FastArray<Collider> _collidersToIgnore = new FastArray<Collider>(); //TODO: Use set here?
    }

    public abstract class GenericTraceLogic<T_Type> : ITraceLogic
        where T_Type : struct, Shapes.IShape
    {
        protected new void reset() {
            base.reset();
            _shape = default;
        }

        protected void setShape(T_Type inShape) {
            _shape = inShape;
        }

        internal T_Type _shape;
    }

    // -------------------------------- Trace ---------------------------------------

    public static Optional<TraceEvent> trace<T_Shape>(GenericTraceLogic<T_Shape> inPointTraceLogic)
        where T_Shape : struct, Shapes.IShape
    {
        if (inPointTraceLogic._sweepShifts.isEmpty()) return new Optional<TraceEvent>();

        using (var __colliders = ClassesStack<FastArray<Collider>>.get())
        using (var __traceState = ClassesStack<XCollisionPrivate.TraceState>.get()){

        int theMask = inPointTraceLogic._traceLayerMask;

        //Debug.Log("{{{");
        __traceState.get().applyIgnores(inPointTraceLogic._collidersToIgnore);
        
        Vector3 theShift = inPointTraceLogic._sweepShifts.getFirstElement().value;
        float theShiftDistance = theShift.magnitude;
        Vector3 theShiftNormal = theShift / theShiftDistance;

        T_Shape theShape = inPointTraceLogic._shape;

        //Debug.Log("-----> " + theMask);
        theShape.overlap(__colliders, theMask);
        foreach (Collider theCollider in __colliders.get()) {
            //Debug.Log(theCollider.gameObject.name + " | "+ theCollider.gameObject.layer);

            __traceState.get().createEventsForEnteredCollider(
                theCollider, 0.0f, theShape, theShiftNormal, theShiftDistance
            );

            var theStartInsideEvent = new TraceEvent(TraceEventType.StartInside, theCollider);
            if (inPointTraceLogic.testFinishCastOnEvent(theStartInsideEvent)) {
                return theStartInsideEvent;
            }
        }

        //Debug.Log("}}}");

        FastArray<Vector3> theShifts = inPointTraceLogic._sweepShifts;

        int theShiftIndex = 0;
        do {
            int theIterationsGuard = 0;

            float theCurrentPosition = 0.0f;
            while (theCurrentPosition != theShiftDistance) {
                T_Shape theCurrentShape = theShape.translated(theShiftNormal * theCurrentPosition);

                RaycastHit theEnterHit;
                Vector3 theLeftShiftDistance = theShiftNormal * (theShiftDistance - theCurrentPosition);
                bool theEnterHitFound = theCurrentShape.trace(out theEnterHit, theLeftShiftDistance, theMask);
                float theEnterEventPosition = theEnterHitFound ?
                        theCurrentPosition + theEnterHit.distance : theShiftDistance;

                Optional<XCollisionPrivate.FutureExitEvent> theNearestEvent = __traceState.get().getNearestExitEvent();
                while (theNearestEvent.isSet() && theNearestEvent.value.position <= theEnterEventPosition) {
                    if (inPointTraceLogic.testFinishCastOnEvent(theNearestEvent.value.traceEvent)) {
                        return theNearestEvent.value.traceEvent;
                    }

                    //TODO: Still ignore until current shift is processed
                    //(it's possible as we trace through convex colliders only)
                    __traceState.get().removeNearestExitEvent();
                    theNearestEvent = __traceState.get().getNearestExitEvent();
                }

                if (theEnterHitFound) {
                    Collider theCollider = theEnterHit.collider;

                    __traceState.get().createEventsForEnteredCollider(
                        theCollider, theEnterEventPosition, theShape, theShiftNormal, theShiftDistance
                    );

                    TraceEvent theEnterEvent = new TraceEvent(TraceEventType.Enter, theCollider, theEnterHit);
                    theEnterEvent.traceDirection = theShiftNormal; //TODO: Put to event initialization
                    if (inPointTraceLogic.testFinishCastOnEvent(theEnterEvent)) {
                        return theEnterEvent;
                    }
                }

                //Debug.Log(theEnterEventDistance);
                theCurrentPosition = theEnterEventPosition;

                if (++theIterationsGuard > 10000) {
                    check(false, "Too many iterations on Trace!");
                    return new Optional<TraceEvent>();
                }
            }

            if (++theShiftIndex >= theShifts.getSize()) {
                break;
            }

            theShape = theShape.translated(theShift);

            theShift = inPointTraceLogic._sweepShifts[theShiftIndex];
            theShiftDistance = theShift.magnitude;
            theShiftNormal = theShift / theShiftDistance;

            __traceState.get().createExitEventsForMediumColliders(theShape, theShiftNormal, theShiftDistance);
        } while(true);

        while (true) {
            Optional<Collider> theMediumCollider = __traceState.get().popLastMediumCollider();
            if (!theMediumCollider.isSet()) break;

            TraceEvent theFinishInsideEvent = new TraceEvent(TraceEventType.FinishInside, theMediumCollider.value);
            if (inPointTraceLogic.testFinishCastOnEvent(theFinishInsideEvent)) {
                return theFinishInsideEvent;
            }
        }

        return new Optional<TraceEvent>();}
    }
}
