﻿
//Frameworks
using UnityEngine;
using System.Collections;
using static XUtils;

//NB: Currently is used only like trigger, without physics
public class XTrigger : MonoBehaviour
{
    //Events
    public System.Action onCollisionInfoIsReady = null;

    //Methods
    //-API
    public FastArray<Collider> colliders {
        get { return _colliders; }
    }

    public void collectOverlapedColliders(FastArray<Collider> outColliders) {
        _frameCollisionInfo.collectOverlapedColliders(outColliders, Time.fixedTime);
    }

    public void collectOverlapedXTriggers(FastArray<XTrigger> outXTriggers) {
        _frameCollisionInfo.collectOverlapedColliders(_ref_colliders, Time.fixedTime);
        foreach (Collider theCollider in _ref_colliders) {
            var theXTrigger = getComponentInParent<XTrigger>(theCollider);
            if (theXTrigger) {
                outXTriggers.add(theXTrigger);
            }
        }
    }
    private FastArray<Collider> _ref_colliders = new FastArray<Collider>();

    public void setIsActive(bool inIsActive) {
        if (_isActive == inIsActive) return;

        AccessPolicy theAccessPolicy = inIsActive ? AccessPolicy.CreateIfNo : AccessPolicy.RemoveIfExist;
        getComponent(this, theAccessPolicy, (Rigidbody inRigidBody) => { inRigidBody.isKinematic = true; });

        _isActive = inIsActive;
    }

    public bool isActive() {
        return _isActive;
    }

    //-Implementation
    private void Awake() {
        initColliders();
        startLateFixedUpdateCoroutine();
    }

    private void initColliders() {
        getComponentsInChildren(_colliders, this);

        foreach (Collider theCollider in colliders) {
            theCollider.isTrigger = true;
        }
    }

    private void OnTriggerEnter(Collider inCollider) {
        //Debug.Log("Enter: " + inCollider.gameObject.name);
        _frameCollisionInfo.addOverlapedCollider(inCollider);
    }

    private void OnTriggerExit(Collider inCollider) {
        //Debug.Log("Exit: " + inCollider.gameObject.name);
        _frameCollisionInfo.removeOverlapedCollider(inCollider);
    }

    private void LateFixedUpdate() {
        _frameCollisionInfo.finalizeForActualTime(Time.fixedTime);
        onCollisionInfoIsReady?.Invoke();
    }

    //--Late Fixed Update logic
    private void startLateFixedUpdateCoroutine() {
        StartCoroutine(lateFixedUpdateCoroutine());
    }

    private IEnumerator lateFixedUpdateCoroutine() {
        while (true) {
            yield return new WaitForFixedUpdate();
            LateFixedUpdate();
        }
    }

    //Fields
    struct FrameCollisionInfo
    {
        public FrameCollisionInfo(bool AS_DEFAULT_CONSTRUCTOR) {
            _actualTimeStamp = new Optional<float>();
            _overlapedColliders = new FastSet<Collider>();
        }

        public void addOverlapedCollider(Collider inCollider) {
            bool theWasAdded = _overlapedColliders.add(verify(inCollider));
            check(theWasAdded);
        }

        public void removeOverlapedCollider(Collider inCollider) {
            bool theWasRemoved = _overlapedColliders.remove(verify(inCollider));
            check(theWasRemoved);
        }

        public void finalizeForActualTime(float inActualTimeStamp) {
            _actualTimeStamp = new Optional<float>(inActualTimeStamp);
        }

        public void collectOverlapedColliders(FastArray<Collider> outColliders, float inActualTimeStampForCheck){
            check(outColliders);

            cleanCollection(_overlapedColliders);
            _overlapedColliders.collectValues(outColliders);
        }

        private Optional<float> _actualTimeStamp;
        private FastSet<Collider> _overlapedColliders;
    }

    //Fields
    private FastArray<Collider> _colliders = new FastArray<Collider>();

    [SerializeField, HideInInspector] bool _isActive = false;

    private FrameCollisionInfo _frameCollisionInfo = new FrameCollisionInfo(true);
}
