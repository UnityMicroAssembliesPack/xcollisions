﻿using static XCollision.Shapes;

//Frameworks
using UnityEngine;

public static partial class XCollision
{
    // ---------------------------------- Point ----------------------------------

    public static bool trace(out RaycastHit outHit,
        Point inPoint, Vector3 inDirection, int inLayerMask= Physics.AllLayers)
    {
        return Physics.Raycast(inPoint.center, inDirection.normalized, out outHit, inDirection.magnitude, inLayerMask);
    }

    public static bool traceSeparate(out RaycastHit outHit,
        Collider inCollider, Point inPoint, Vector3 inDirection)
    {
        return inCollider.Raycast(new Ray(inPoint.center, inDirection.normalized), out outHit, inDirection.magnitude);
    }

    // ---------------------------------- Sphere ---------------------------------

    public static bool trace(out RaycastHit outHit,
        Sphere inSphere, Vector3 inDirection, int inLayerMask = Physics.AllLayers)
    {
        if (inSphere.isZero()) {
            return new Point(inSphere.center).trace(out outHit, inDirection, inLayerMask);
        } else {
            return Physics.SphereCast(
                inSphere.center, inSphere.radius, inDirection.normalized, out outHit, inDirection.magnitude,
                inLayerMask
            );
        }
    }

    public static bool traceSeparate(out RaycastHit outHit,
        Collider inCollider, Sphere inSphere, Vector3 inDirection)
    {
        if (inSphere.isZero()) {
            return new Point(inSphere.center).traceSeparate(out outHit, inCollider, inDirection);
        } else {
            int theSavedLayer = inCollider.gameObject.layer;
            inCollider.gameObject.layer = kSeparateTraceLayer;
            bool theHitFound = trace(out outHit, inSphere, inDirection, kSeparateTraceLayer);
            inCollider.gameObject.layer = theSavedLayer;
            return theHitFound;
        }
    }

    // ---------------------------------- Capsule -------------------------------------

    public static bool trace(out RaycastHit outHit,
        Capsule inCapsule, Vector3 inDirection, int inLayerMask = Physics.AllLayers)
    {
        if (inCapsule.isSphere()) {
            return new Sphere(inCapsule.point1, inCapsule.radius).trace(out outHit, inDirection, inLayerMask);
        } else {
            return Physics.CapsuleCast(inCapsule.point1, inCapsule.point2, inCapsule.radius,
                inDirection.normalized, out outHit, inDirection.magnitude,
                inLayerMask
            );
        }
    }

    public static bool traceSeparate(out RaycastHit outHit,
        Collider inCollider, Capsule inCapsule, Vector3 inDirection)
    {
        if (inCapsule.isSphere()) {
            return new Sphere(inCapsule.point1, inCapsule.radius).traceSeparate(out outHit, inCollider, inDirection);
        } else {
            int theSavedLayer = inCollider.gameObject.layer;
            inCollider.gameObject.layer = kSeparateTraceLayer;
            bool theHitFound = trace(out outHit, inCapsule, inDirection, kSeparateTraceLayer);
            inCollider.gameObject.layer = theSavedLayer;
            return theHitFound;
        }
    }

    // ---------------------------------- Box -------------------------------------

    public static bool trace(out RaycastHit outHit,
        Box inBox, Vector3 inDirection, int inLayerMask = Physics.AllLayers)
    {
        if (inBox.isZero()) {
            return new Point(inBox.center).trace(out outHit, inDirection, inLayerMask);
        } else {
            return Physics.BoxCast(
                inBox.center, inBox.halfExtents, inDirection.normalized, out outHit, inBox.rotation,
                inDirection.magnitude, inLayerMask
            );
        }
    }

    public static bool traceSeparate(out RaycastHit outHit,
        Collider inCollider, Box inBox, Vector3 inDirection)
    {
        int theSavedLayer = inCollider.gameObject.layer;
        inCollider.gameObject.layer = kSeparateTraceLayer;
        bool theHitFound = trace(out outHit, inBox, inDirection, kSeparateTraceLayer);
        inCollider.gameObject.layer = theSavedLayer;
        return theHitFound;
    }
}
