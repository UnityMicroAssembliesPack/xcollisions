﻿using UnityEngine;

internal static partial class XCollisionPrivate
{
    public static void preventUsingUtilityLayers(ref LayerMask inTraceLayerMask) {
        int theLayerMask = 1 << XCollision.kIgnoreLayer;
        if (0 != (inTraceLayerMask & theLayerMask)) {
            //&& inTraceLayerMask != Physics.AllLayers

            //Debug.Log("Ignore layer [" + kIgnoreLayer + "] used on trace mask. It was reseted. " +
            //    "Please, set XCollisions.kIgnoreLayer to Layer value that is not used in game logic"
            //);
            inTraceLayerMask = inTraceLayerMask & ~theLayerMask;
        }
    
        theLayerMask = 1 << XCollision.kSeparateTraceLayer;
        if (0 != (inTraceLayerMask & theLayerMask)) {
            //Debug.Log("Ignore layer [" + kSeparateTraceLayer + "] used on trace mask. It was reseted. " +
            //    "Please, set XCollisions.kSeparateTraceLayer to Layer value that is not used in game logic"
            //);
            inTraceLayerMask = inTraceLayerMask & ~theLayerMask;
        }
    }

    public static void checkCollidersNum(int theActualCollidersNum) {
        if (theActualCollidersNum > kMaxCollidersToTrack) {
            Debug.Log("To many colliders requested to process");
        }
    }

    public static void checkCollider(Collider inCollider) {
        XUtils.check(inCollider, "XCollisionPrivate.checkCollider: Null collider");
        //TODO: Make more scale checks
        Vector3 theScale = inCollider.transform.lossyScale;
        XUtils.check(
            XMath.equalsWithPrecision(theScale.x, theScale.y) && XMath.equalsWithPrecision(theScale.x, theScale.z),
            "XCollisionPrivate.checkCollider: Not linear scale"
        );
    }
}
