﻿using static XCollision.Shapes;

//Frameworks
using UnityEngine;
using static XUtils;

public static partial class XCollision
{
    // ---------------------------------- Point ----------------------------------

    public static void overlap(FastArray<Collider> outColliders,
        Point inPoint, int inLayerMask = Physics.AllLayers)
    {
        using (var __regColliders = ArraysStack<Collider>.get(XCollisionPrivate.kMaxCollidersToTrack)){

        int theActualCollidersNum = Physics.OverlapSphereNonAlloc(
            inPoint.center, XCollisionPrivate.kMinimalSphereRadius, __regColliders, inLayerMask
        );
        XCollisionPrivate.checkCollidersNum(theActualCollidersNum);
                
        outColliders.set(__regColliders, theActualCollidersNum, false);}
    }

    // ---------------------------------- Sphere ---------------------------------

    public static void overlap(FastArray<Collider> outColliders,
        Sphere inSphere, int inLayerMask = Physics.AllLayers)
    {
        if (inSphere.isZero()) {
            new Point(inSphere.center).overlap(outColliders, inLayerMask);
        } else {
            using (var __regColliders = ArraysStack<Collider>.get(XCollisionPrivate.kMaxCollidersToTrack)) {
                int theActualCollidersNum = Physics.OverlapSphereNonAlloc(
                    inSphere.center, inSphere.radius, __regColliders, inLayerMask
                );
                XCollisionPrivate.checkCollidersNum(theActualCollidersNum);
                outColliders.set(__regColliders, theActualCollidersNum, false);
            }
        }
    }

    // ---------------------------------- Capsule -------------------------------------

    public static void overlap(FastArray<Collider> outColliders,
        Capsule inCapsule, int inLayerMask = Physics.AllLayers)
    {
        if (inCapsule.isSphere()) {
            new Sphere(inCapsule.point1, inCapsule.radius).overlap(outColliders, inLayerMask);
        } else {
            using (var __regColliders = ArraysStack<Collider>.get(XCollisionPrivate.kMaxCollidersToTrack)) {
            int theActualCollidersNum = Physics.OverlapCapsuleNonAlloc(
                inCapsule.point1, inCapsule.point2, inCapsule.radius, __regColliders, inLayerMask
            );
            outColliders.set(__regColliders, theActualCollidersNum, false);}
        }
    }    

    // ---------------------------------- Box -------------------------------------

    public static void overlap(FastArray<Collider> outColliders,
        Box inBox, int inLayerMask = Physics.AllLayers)
    {
        if (inBox.isZero()) {
            new Point(inBox.center).overlap(outColliders, inLayerMask);
        } else {
            using (var __regColliders = ArraysStack<Collider>.get(XCollisionPrivate.kMaxCollidersToTrack)) {
            int theActualCollidersNum = Physics.OverlapBoxNonAlloc(
                inBox.center, inBox.halfExtents, __regColliders, inBox.rotation, inLayerMask
            );
            outColliders.set(__regColliders, theActualCollidersNum, false);}
        }
    }
}
