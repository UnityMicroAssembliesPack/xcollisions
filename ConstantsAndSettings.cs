﻿using UnityEngine;
using static XMath;

public static partial class XCollision
{
    public static LayerMask AllLayers { get { return Physics.AllLayers; } }

    public static int kIgnoreLayer = kDefaultIgnoreLayer;
    public static int kSeparateTraceLayer = 30;

    public static int kDefaultIgnoreLayer { get { return safeCast(log2(safeCast(Physics.IgnoreRaycastLayer))); } }
}
