﻿using UnityEngine;
using XMathTypes;

public class IsolatedRelativeObjectsGroup : MonoBehaviour
{
    //Iterate object attaches using child objects iteration

    internal Vector3 getGroupPositionFor(Vector3 inWorldPosition) {
        return _separateRootPosition + transform.InverseTransformPoint(inWorldPosition);
    }

    internal LocatorTransform getGroupLocatorTransform(LocatorTransform inWorldLocatorTransform) {
        LocatorTransform theThisLocatorTransform = new LocatorTransform(transform);
        LocatorTransform theResult = theThisLocatorTransform.getLocalTransform(inWorldLocatorTransform);
        theResult.position += _separateRootPosition;
        return theResult;
    }

    [SerializeField] private Vector3 _separateRootPosition = new Vector3(10.0f, 10.0f, 10.0f);
}
