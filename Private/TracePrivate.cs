﻿using UnityEngine;

internal static partial class XCollisionPrivate
{
    public struct ColliderIgnore {
        public ColliderIgnore(Collider inCollider) {
            _collider = inCollider;
            _maskToRestore = inCollider.gameObject.layer;
            inCollider.gameObject.layer = XCollision.kIgnoreLayer;

            //Debug.Log("[!!!IGNORE: " + _collider.gameObject.name + "]: " + inCollider.gameObject.layer);
        }

        public void dispose() {
            restoreCollision();
        }

        private void restoreCollision() {
            //Debug.Log("[!!!RESTORE: " + _collider.gameObject.name + "]: " + _maskToRestore);
            _collider.gameObject.layer = _maskToRestore;
        }

        internal Collider _collider; //XCollisionsPrivate access only
        private int _maskToRestore;
    }

    public struct MediumColliderInfo {
        public MediumColliderInfo(Collider inCollider) {
            _colliderIgnore = new ColliderIgnore(inCollider);
        }

        public Collider collider { get { return _colliderIgnore._collider; } }

        public void dispose() {
            _colliderIgnore.dispose();
        }

        private ColliderIgnore _colliderIgnore;
    }

    public struct FutureExitEvent {
        public FutureExitEvent(float inPosition, RaycastHit inHit) {
            _position = inPosition;
            _event = new XCollision.TraceEvent(XCollision.TraceEventType.Exit, inHit.collider, inHit);
            _colliderIgnore = new ColliderIgnore(inHit.collider);
        }   

        public float position { get { return _position; } }
        public XCollision.TraceEvent traceEvent { get { return _event; } }

        public void dispose() {
            _colliderIgnore.dispose();
        }

        private float _position;
        private XCollision.TraceEvent _event;
        private ColliderIgnore _colliderIgnore;
    }


    public class TraceState : System.IDisposable
    {    
        //Initialization
        public void applyIgnores(FastArray<Collider> inCollidersToIgnore) {
            foreach (Collider theColliderToIgnore in inCollidersToIgnore) {
                _ignores.add(new ColliderIgnore(theColliderToIgnore));
                //Debug.Log(theColliderToIgnore.gameObject.name + " | " + theColliderToIgnore.gameObject.layer);
            }
        }

        //Medium tracking
        public void createEventsForEnteredCollider<T_Type>(
            Collider inCollider, float inEnterEventPosition,
            T_Type inShape, Vector3 inShiftNormal, float inShiftDistance)
            where T_Type : struct, XCollision.Shapes.IShape
        {
            T_Type theBackShiftShape = inShape.translated(inShiftNormal * inShiftDistance);
            float theBackShiftDistance = inShiftDistance - inEnterEventPosition;

            RaycastHit theHit;
            if (!theBackShiftShape.traceSeparate(out theHit, inCollider, -inShiftNormal * theBackShiftDistance)) {
                addMediumCollider(inCollider);
            } else {
                addExitEvent(inShiftDistance - theHit.distance, theHit);
            }
        }

        private void addMediumCollider(Collider inCollider) {
            medium.add(new MediumColliderInfo(inCollider));
        }

        public void createExitEventsForMediumColliders<T_Type>(
            T_Type inShape, Vector3 inShiftNormal, float inShiftDistance)
            where T_Type : struct, XCollision.Shapes.IShape
        {
            T_Type theBackShiftShape = inShape.translated(inShiftNormal * inShiftDistance);
            Vector3 theBackShift = -inShiftNormal * inShiftDistance;

            medium.iterateWithRemove((MediumColliderInfo theMediumCollider) => {
                Collider theCollider = theMediumCollider.collider;
                RaycastHit theHit;
                if (!theBackShiftShape.traceSeparate(out theHit, theCollider, theBackShift)) return false;

                theMediumCollider.dispose();
                addExitEvent(inShiftDistance - theHit.distance, theHit);
                return true;
            }, true);
        }

        public Optional<Collider> popLastMediumCollider() {
            Optional<MediumColliderInfo> theMediumCollider = medium.getLastElement();
            if (!theMediumCollider.isSet()) return new Optional<Collider>();

            theMediumCollider.value.dispose();
            medium.removeLastElement();
            return theMediumCollider.value.collider;
        }

        private void addExitEvent(float inPosition, RaycastHit inHit) {
            XUtils.addSorted(futureExits, new FutureExitEvent(inPosition, inHit), gHistoryComparator);
        }

        //Exit events
        public Optional<FutureExitEvent> getNearestExitEvent() {
            return futureExits.getLastElement();
        }

        public void removeNearestExitEvent() {
            futureExits.getLastElement().value.dispose();
            futureExits.removeLastElement();
        }

        public void Dispose() {
            foreach (ColliderIgnore theIgnore in _ignores) {
                theIgnore.dispose();
            }
            _ignores.clear(false);

            foreach (MediumColliderInfo theMediumElement in medium) {
                theMediumElement.dispose();
            }
            medium.clear(false);

            foreach (FutureExitEvent theFutureExit in futureExits) {
                theFutureExit.dispose();
            }
            futureExits.clear(false);
        }

        private FastArray<ColliderIgnore> _ignores = new FastArray<ColliderIgnore>();
        public FastArray<MediumColliderInfo> medium = new FastArray<MediumColliderInfo>();
        public FastArray<FutureExitEvent> futureExits = new FastArray<FutureExitEvent>();
    }

    private static System.Func<FutureExitEvent, FutureExitEvent, XUtils.Comparation> gHistoryComparator =
        (FutureExitEvent inEventA, FutureExitEvent inEventB) =>
    {
        //NB: Inverse sort used for poping last elements
        return XUtils.compare(inEventB.position, inEventA.position);
    };
}
